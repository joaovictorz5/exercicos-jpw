
/*6. Crie pacote usando npm e implemente um script que receba argumentos, como
node index.js 1 2 3 4 5 6 . O script deve retornar a soma total apenas dos
argumentos divisíveis por 2.*/

var argumentos = process.argv;

var total = 0;
argumentos.forEach(element => {
    if(element % 2 == 0){
        total += parseInt(element);
    }
    
});
console.log(total);


/*
27. Implemente um pacote capaz de solicitar ao usuário uma sequência de palavras.
Ao final, o programa deve salvar todas as palavras em um arquivo de texto (de
preferência no formato JSON ). No entanto, o arquivo não poderá conter palavras
repetidas ou nulas.
*/
/*const fs = require('fs')

var arquivo = fs.readFileSync("AUTHOR.md", "utf8",) // leitura de arquivo
console.log(arquivo)*/

/*
// FARA A LEITURA DO "DEPOIS", pois a leitura do arquivo é assincrona.
var arquivo = fs.readFile("AUTHOR.md", "utf8", (err, arquivo) =>{
    if(!err){
        console.log(arquivo)
    }
})

console.log("depois")

// grava o texto de conteudo em um arquivo
var conteudo = {"nome": "Joao Victor", "Profissão": "Desempregado"};

var Option = {
    encoding: "utf8",
    flag: "w"
}
var arquivo = fs.writeFile("AUTHOR.md", JSON.stringify(conteudo), Option, (err) =>{
    if(err){
        console.log(err)
    }
})
*/
/*
console.log("depois") */